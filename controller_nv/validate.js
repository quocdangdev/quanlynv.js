
function kiemTraRong(value, idError) {
    if (value.length == 0) {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "trường này ko để rổng";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function checkLength(value, idError) {
    if (value.length < 7 && value.length > 3) {
        document.getElementById(idError).innerText = "";
        return true;
    }
    else {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "trường này ko hợp lệ";
        return false;
    }

}
function kiemTraTKNV(idSV, listSv, idError) {
    var index = listSv.findIndex(function (nv) {
        return nv.tk == idSV;
    });
    if (index == -1) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "tk này đã tồn tại";
        return false;
    }

}
function kiemTraEmail(value, idError) {
    const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = re.test(value);
    if (!isEmail) {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "email này ko hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function kiemTraTen(value, idError) {
    const result = /^[a-z\s]+$/i
    var isName = result.test(value)
    if (isName) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "tên này ko hợp lệ";
        return false;
    }
}
function kiemTraPasswd(value, idError) {
    var regularExpression = new RegExp(
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9_])"
    );
    var result = regularExpression.test(value);
    if (!result) {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "pass này ko hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function kiemTraNgayThangNam(value, idError) {
    var reGoodDate = new RegExp("^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$")
    var isDay = reGoodDate.test(value)
    if (!isDay) {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "ngày tháng năm  này ko hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function kiemTraLuong(value, idError) {
    if (value <= 20000000 && value >= 1000000) {
        document.getElementById(idError).innerText = "";
        return true;
    }
    else {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "trường này ko hợp lệ";
        return false;
    }
}
function kiemTraChucVu(value, idError) {
    if (value == 'Chọn chức vụ') {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "vui lòng chọn chức vụ";
        return false;
    }
    else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function kiemTraGioLam(value, idError) {
    if (value <= 200 && value >= 80) {
        document.getElementById(idError).innerText = "";
        return true;
    }
    else {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "trường này ko hợp lệ";
        return false;
    }
}


