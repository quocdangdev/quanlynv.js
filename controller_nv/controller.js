function layThongTinTuform() {
    var taiKhoan = document.getElementById("tknv").value.trim();
    var tenNv = document.getElementById("name").value.trim();
    var email = document.getElementById("email").value.trim();
    var matKhau = document.getElementById("password").value.trim();
    var ngayLam = document.getElementById("datepicker").value.trim();
    var luongCoBan = document.getElementById("luongCB").value.trim();
    var select = document.getElementById('chucvu');
    var option = select.options[select.selectedIndex]
    var chucVu = option.value;
    var gioLam = document.getElementById("gioLam").value.trim();

    var nv = new NhanVien(taiKhoan, tenNv, email, matKhau, ngayLam, luongCoBan, chucVu, gioLam)
    return nv;
}
function randerDSNV(list) {
    var contentHTML = ""
    for (var i = 0; i < list.length; i++) {
        var dsNvHienTai = list[i];
        var contentTr = `
        <tr>
        <td> ${dsNvHienTai.tk} </td>
        <td> ${dsNvHienTai.ten} </td>
        <td> ${dsNvHienTai.email} </td>
        <td> ${dsNvHienTai.ngayLam} </td>
        <td> ${dsNvHienTai.chucVu} </td>
        <td> ${dsNvHienTai.tongLuong()} </td>
        <td> ${dsNvHienTai.xepLoai()} </td>
        <td>
        <button onclick="xoaNv('${dsNvHienTai.tk}')" class="btn btn-danger" >xóa</button>
        <button onclick="suaNv('${dsNvHienTai.tk}')" class="btn btn-primary" id="btnThem" data-toggle="modal"
        data-target="#myModal">sửa</button> 
        </td>
        </tr>
        `;
        contentHTML += contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function showThongTinLenForm(nv) {
    document.getElementById("tknv").value = nv.tk;
    document.getElementById("name").value = nv.ten;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.matKhau;
    document.getElementById("datepicker").value = nv.ngayLam;
    document.getElementById("luongCB").value = nv.luongCB
    document.getElementById('chucvu').value = nv.chucVu
    document.getElementById("gioLam").value = nv.gioLam
}